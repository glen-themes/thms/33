![Screenshot preview of the theme "Sparrow" by glenthemes](https://64.media.tumblr.com/c02004c33d11c576a490450ab1d5a327/tumblr_pebgwoqvXO1ubolzro2_r1_1280.png)

**Theme no.:** 33  
**Theme name:** Sparrow  
**Theme type:** Free / Tumblr use  
**Description:** A fun theme: ~~I need healing!~~ A tribute to all the exhausted healer mains out there. This theme is inspired by Overwatch’s UI, and features Genji Shimada.  
**Author:** @&hairsp;glenthemes  

**Release date:** 2018-08-31

**Post:** [glenthemes.tumblr.com/post/177584436099](https://glenthemes.tumblr.com/post/177584436099)  
**Preview:** [glenthpvs.tumblr.com/sparrow](https://glenthpvs.tumblr.com/sparrow)  
**Download:** [pastebin.com/9YrB9BWc](https://pastebin.com/9YrB9BWc)
